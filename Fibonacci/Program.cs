﻿using System;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--- Fibonacci-számok ---");
            Console.Write("Sorozat-elemek száma: ");
            int quantity = Convert.ToInt32(Console.ReadLine());

            Fibonacci(quantity);
            
            Console.WriteLine("\nA kilépéshez kérem nyomjon meg egy billentyűt!");
            Console.ReadKey();
        }

        static void Fibonacci(int quantity = 1, ulong x = 0, ulong y = 1, int counter = 0)
        {
            if (counter < quantity)
            {
                Console.Write("{0}, ", x);
                Fibonacci(quantity, y, y + x, ++counter);
            }
        }

    }
}